
source_imp = [
    ('CA', 'County','Alameda', 'http://www.ACGOV.ORG/'),
    ('CA', 'County','Alpine', 'http://www.ALPINECOUNTYCA.GOV/'),
    ('CA', 'County','Amador', 'http://www.CO.AMADOR.CA.US/'),
    ('CA', 'County','Butte', 'http://www.BUTTECOUNTY.NET/'),
    ('CA', 'County','Calaveras Country', 'http://www.CO.CALAVERAS.CA.US/'),
    ('CA', 'County','Colusa', 'http://www.OFCOLUSA.ORG/'),
    ('CA', 'County','Contra Costa', 'http://www.CO.CONTRA-COSTA.CA.US/'),
    ('CA', 'County','Del Norte', 'http://www.CO.DEL-NORTE.CA.US/'),
    ('CA', 'County','El Dorado', 'http://www.EDCGOV.US/'),
    ('CA', 'County','Fresno', 'http://www.CO.FRESNO.CA.US /'),
    ('CA', 'County','Glenn', 'http://www.COUNTYOFGLENN.NET/'),
    ('CA', 'County','Humboldt', 'http://www.CO.HUMBOLDT.CA.US/'),
    ('CA', 'County','Imperial', 'http://www.CO.IMPERIAL.CA.US/'),
    ('CA', 'County','Inyo', 'http://www.INYOCOUNTY.US/'),
    ('CA', 'County','Kern', 'http://www.CO.KERN.CA.US/'),
    ('CA', 'County','Kings', 'http://www.COUNTYOFKINGS.COM /'),
    ('CA', 'County','Lake', 'http://www.CO.LAKE.CA.US /'),
    ('CA', 'County','Lassen', 'http://www.CO.LASSEN.CA.US/'),
    ('CA', 'County','Los Angeles', 'http://www.LACOUNTY.GOV/'),
    ('CA', 'County','Madera', 'http://www.MADERA-COUNTY.COM/'),
    ('CA', 'County','Marin', 'http://www.MARINCOUNTY.ORG/'),
    ('CA', 'County','Mariposa', 'http://www.MARIPOSACOUNTY.ORG/'),
    ('CA', 'County','Mendocino', 'http://www.CO.MENDOCINO.CA.US/'),
    ('CA', 'County','Merced', 'http://www.CO.MERCED.CA.US/'),
    ('CA', 'County','Modoc', 'http://www.CO.MODOC.CA.US/'),
    ('CA', 'County','Mono', 'http://www.MONOCOUNTY.CA.GOV/'),
    ('CA', 'County','Monterey', 'http://www.CO.MONTEREY.CA.US/'),
    ('CA', 'County','Napa', 'http://www.COUNTYOFNAPA.ORG/'),
    ('CA', 'County','Nevada', 'http://www.MYNEVADACOUNTY.COM/'),
    ('CA', 'County','Orange', 'http://www.OCGOV.COM/'),
    ('CA', 'County','Placer', 'http://www.PLACER.CA.GOV/'),
    ('CA', 'County','Plumas', 'http://www.COUNTYOFPLUMAS.COM/'),
    ('CA', 'County','Riverside', 'http://www.CO.RIVERSIDE.CA.US/'),
    ('CA', 'County','Sacramento', 'http://www.SACCOUNTY.NET/'),
    ('CA', 'County','San Benito', 'http://www.SAN-BENITO.CA.US/'),
    ('CA', 'County','San Bernardino', 'http://www.SBCOUNTY.GOV/DEFAULT.ASP/'),
    ('CA', 'County','San Diego', 'http://www.CO.SAN-DIEGO.CA.US/'),
    ('CA', 'County','San Francisco', 'HTTP://SFGOV.ORG/'),
    ('CA', 'County','San Joaquin', 'http://www.CO.SAN-JOAQUIN.CA.US/'),
    ('CA', 'County','San Luis Obispo', 'http://www.SLOCOUNTY.CA.GOV/'),
    ('CA', 'County','San Mateo', 'http://www.CO.SANMATEO.CA.US/'),
    ('CA', 'County','Santa Barbara', 'http://www.COUNTYOFSB.ORG/'),
    ('CA', 'County','Santa Clara', 'http://www.SCCGOV.ORG/'),
    ('CA', 'County','Santa Cruz', 'http://www.CO.SANTA-CRUZ.CA.US/'),
    ('CA', 'County','Shasta', 'http://www.CO.SHASTA.CA.US/'),
    ('CA', 'County','Sierra', 'http://www.SIERRACOUNTY.CA.GOV/'),
    ('CA', 'County','Siskiyou', 'http://www.CO.SISKIYOU.CA.US/'),
    ('CA', 'County','Solano', 'http://www.CO.SOLANO.CA.US/'),
    ('CA', 'County','Sonoma', 'http://www.SONOMA-COUNTY.ORG/'),
    ('CA', 'County','Stanislaus', 'http://www.CO.STANISLAUS.CA.US/'),
    ('CA', 'County','Sutter', 'http://www.CO.SUTTER.CA.US/'),
    ('CA', 'County','Tehama', 'http://www.CO.TEHAMA.CA.US/'),
    ('CA', 'County','Trinity', 'http://www.TRINITYCOUNTY.ORG/'),
    ('CA', 'County','Tulare', 'http://www.CO.TULARE.CA.US/'),
    ('CA', 'County','Tuolumne', 'http://www.TUOLUMNECOUNTY.CA.GOV/'),
    ('CA', 'County','Ventura', 'http://www.COUNTYOFVENTURA.ORG/'),
    ('CA', 'County','Yolo', 'http://www.YOLOCOUNTY.ORG/'),
    ('CA', 'County','Yuba', 'http://www.CO.YUBA.CA.US/')]

from civicwatch import db, models
from datetime import datetime, timedelta

def import_sources():
    t = datetime.utcnow()
    for s in source_imp:
        t = t + timedelta(minutes=30)
        state, mun_type, name, url = s
        source = models.Source()
        source.name = "{} {}".format(name, mun_type)
        source.url = url.lower()
        source.state = state
        source.county = name
        source.next_run = t

        db.session.add(source)
        db.session.commit()

if __name__ == "__main__":
    import_sources()
