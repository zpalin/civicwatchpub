#!/usr/bin/env python
import os
from civicwatch import app, db, models
from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand
from civicwatch.auth import CreateUser

manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    return dict(app=app, db=db, models=models)

manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)
manager.add_command('createuser', CreateUser)

if __name__ == '__main__':
    manager.run()
