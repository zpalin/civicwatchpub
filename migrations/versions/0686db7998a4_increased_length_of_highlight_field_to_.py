"""increased length of highlight field to 400

Revision ID: 0686db7998a4
Revises: 5d6da83b67e2
Create Date: 2016-11-17 10:22:52.825391

"""

# revision identifiers, used by Alembic.
revision = '0686db7998a4'
down_revision = '5d6da83b67e2'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('results', 'highlight', existing_type=sa.String(length=300), type_=sa.String(length=400))

def downgrade():
    op.alter_column('results', 'highlight', existing_type=sa.String(length=400), type_=sa.String(length=300))
