all: clean lint test

req:
	pip install -r requirements.txt
	npm install

dev: req
	gulp watch

test:
	py.test

lint:
	pylint helloworld -r n

cov: coverage
coverage:
	coverage erase
	coverage run --source helloworld -m pytest
	coverage report

clean:
	find . -name \*.pyc -o -name \*.pyo -o -name __pycache__ -exec rm -rf {} +

start_workers:
	celery multi start 4 -A civicwatch.celery --loglevel=info -P gevent -c 100

restart_workers:
	celery multi restart 4 -A civicwatch.celery

kill_workers:
	celery multi kill 4 -A civicwatch.celery

start_crawls:
	PATCH_GEVENT=true celery multi start 8 -A civicwatch.celery --loglevel=info -Q crawls -P gevent -c 100

restart_crawls:
	PATCH_GEVENT=true celery multi restart 8 -A civicwatch.celery -Q crawls

kill_crawls:
	celery multi kill 8 -A civicwatch.celery -Q crawls

check_workers:
	celery inspect active -A civicwatch.celery

beat:
	celery -A civicwatch.celery beat

flower:
	flower -A civicwatch.celery

prod-start:
	uwsgi --ini uwsgi.ini

prod-stop:
	uwsgi --stop /tmp/uwsgi.pid

prod-restart:
	uwsgi --reload /tmp/uwsgi.pid

build:
	NODE_ENV=production gulp
