import os
import sys
from gevent import monkey
from flask import Flask, redirect
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_redis import FlaskRedis
from flask_admin import Admin
from flask_login import LoginManager
from .config import config
from elasticsearch_dsl.connections import connections
from celery import Celery

app = Flask(__name__)

config_object = config[os.getenv('FLASK_CONFIG') or 'default']
app.config.from_object(config_object)

if os.getenv('PATCH_GEVENT') == 'true':
    monkey.patch_all()

connections.create_connection(
    hosts=[config_object.ELASTIC_SERVER],
    http_auth=(config_object.ELASTIC_USERNAME, config_object.ELASTIC_PASSWORD),
    retry_on_timeout=True)

mail = Mail(app)
db = SQLAlchemy(app)
redis_store = FlaskRedis(app)

login_manager = LoginManager(app)
login_manager.login_view = 'login'

# Celery Config
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

TaskBase = celery.Task
class ContextTask(TaskBase):
    abstract = True
    def __call__(self, *args, **kwargs):
        with app.app_context():
            return TaskBase.__call__(self, *args, **kwargs)
celery.Task = ContextTask

from . import auth
from . import models
from . import tasks
from . import admin
from . import views
from . import api
