from civicwatch import models, app, db, config_object
from flask_login import current_user
from flask_restful import Resource, Api, abort, reqparse
from functools import wraps
from flask import request
import sys
import json

api = Api(app)

def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated:
            return func(*args, **kwargs)
        else:
            abort(401)
    return wrapper

class AuthResource(Resource):
    method_decorators = [authenticate]


feed_parser = reqparse.RequestParser()
feed_parser.add_argument('alert', type=bool)
feed_parser.add_argument('terms', default='[]')
feed_parser.add_argument('title')

getparse = reqparse.RequestParser()
getparse.add_argument('include_results', type=bool, default=True, location='args')

class FeedIndex(AuthResource):
    def get(self):
        include_results = request.args.get('include_results', 'true').lower() == 'true'
        return [f.serialize(include_results) for f in current_user.feeds]

    def post(self):
        args = feed_parser.parse_args()
        feed = models.Feed(title=args['title'], alert=args['alert'], user=current_user)

        terms = json.loads(args['terms'])
        feed.update_terms(terms)

        db.session.add(feed)
        db.session.commit()
        return feed.serialize()


def get_feed(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        feed = models.Feed.query.get(kwargs.get('feed_id'))
        if not feed or feed.user != current_user:
            abort(301)

        del kwargs['feed_id']
        kwargs['feed'] = feed

        return func(*args, **kwargs)
    return wrapper

class FeedDetail(AuthResource):
    method_decorators = [get_feed, authenticate]

    def get(self, feed):
        return feed.serialize()

    def delete(self, feed):
        db.session.delete(feed)
        return 'ok'

    def put(self, feed):
        if feed.user != current_user:
            abort(403)

        args = feed_parser.parse_args()
        feed.alert = args.get('alert', feed.alert)
        feed.title = args.get('title', feed.title)

        terms = json.loads(args['terms'])
        feed.update_terms(terms)

        db.session.add(feed)
        db.session.commit()

        return feed.serialize()

api.add_resource(FeedDetail, '/api/feeds/<feed_id>')
api.add_resource(FeedIndex, '/api/feeds')
