from . import login_manager
from . import config_object
from . import models, db
import getpass
from flask_script import Command


@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(user_id)


class CreateUser(Command):
    "Creates user"

    def run(self):
        email = input("Email: ")
        password = getpass.getpass("Password: ")
        password_conf = getpass.getpass("Password (confirmation): ")

        if password == password_conf:
            user = models.User(email=email, password=password)
            db.session.add(user)
            db.session.commit()
        else:
            print("Passwords didn't match.")
