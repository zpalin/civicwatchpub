import json
from flask import render_template, url_for, redirect, request, flash
from . import app, config_object, login_manager
from flask_login import login_required, login_user, logout_user, current_user
from .models import User


@app.route('/<path:path>')
@login_required
def catch_all(path):
    return render_template('frontend.j2')


@app.route('/')
def landing():
    return render_template('landing.j2')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if(current_user and current_user.is_authenticated):
        return redirect(request.args.get('next') or '/dashboard')

    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            login_user(user)

            return redirect(request.args.get('next') or '/dashboard')
        flash('Invalid username or password.', 'error')

    return render_template('login.j2')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/api/test')
def api_test():
    x = request.args.get('include_results')
    if x.lower() == 'true':
        return x
    else:
        return 'not {}'.format(x)
