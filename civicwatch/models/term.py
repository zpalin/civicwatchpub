from datetime import datetime
from civicwatch import db
from .normalizedterm import NormalizedTerm

class Term(db.Model):
    __tablename__ = 'terms'
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(64))
    normalized_term_id = db.Column(db.Integer, db.ForeignKey('normalizedterms.id'))
    normalized_term = db.relationship('NormalizedTerm', foreign_keys=[normalized_term_id])
    feed_id = db.Column(db.Integer, db.ForeignKey('feeds.id'), index=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "Term(id={}, label='{}')".format(self.id, self.label)

    @property
    def results(self):
        return self.normalized_term.results

    def associate_normalized(self):
        normterm = NormalizedTerm.find_or_create(self.label)
        self.normalized_term = normterm
        self.normalized_term_id = normterm.id

    def serialize(self):
        return {
            'id': self.id,
            'label': self.label
        }

#
# @db.event.listens_for(Term, 'before_insert')
# def after_insert_listener(mapper, connection, target):
#     normterm =
