from datetime import datetime
from sqlalchemy import desc
from civicwatch import db
from .term import Term
from .result import Result
import sys

class Feed(db.Model):
    __tablename__ = 'feeds'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200))
    terms = db.relationship('Term', backref='feed', foreign_keys=[Term.feed_id])
    created = db.Column(db.DateTime, default=datetime.utcnow)
    alert = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), index=True)


    def __repr__(self):
        return "Feed(id={}, title='{}')".format(self.id, self.title)

    def update_terms(self, terms):
        for t in self.terms:
            db.session.delete(t)
            db.session.commit()

        for t in terms:
            label = t.get('label')
            if label:
                term_record = Term()
                term_record.label = label
                term_record.associate_normalized()
                term_record.feed = self
                db.session.add(term_record)
                db.session.commit()


    def results(self, limit=100, offset=0):
        nt_ids = [t.normalized_term_id for t in self.terms]

        if not len(nt_ids):
            return []

        q = Result.query
        q = q.filter(Result.normalized_term_id.in_(nt_ids))
        q = q.order_by(desc(Result.date))
        q = q.limit(limit)
        q = q.offset(offset)
        return q.all()

    def serialize(self, include_results=True):
        serialized = {
            'id': self.id,
            'title': self.title,
            'alert': self.alert,
            'terms': [t.serialize() for t in self.terms],
        }

        if include_results:
            serialized['results'] = [r.serialize() for r in self.results()]

        return serialized
