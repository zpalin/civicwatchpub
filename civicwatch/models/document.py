from elasticsearch_dsl import DocType, String, Date, Boolean, Q, MetaField
from datetime import datetime
from civicwatch.tasks import check_document_terms
from civicwatch import config_object

class Text(String):
    name = 'text'

class Document(DocType):
    url = String(index='not_analyzed')
    body = Text(term_vector='with_positions_offsets', analyzer='english')
    created_at = Date()
    doc_type = String(index='not_analyzed')
    crawl_id = String(index='not_analyzed')

    def __repr__(self):
        return "Document(url='{}')".format(self.url)

    class Meta:
        all = MetaField(enabled=False)
        index = config_object.ELASTIC_INDEX

    def is_saved(self):
        return hasattr(self.meta, 'id')

    def old_records(self, exclude_self=True, offset=0, limit=10):
        f = Q('match', url=self.url)

        if exclude_self and self.is_saved():
            f = f & ~Q('ids', values=[self.meta.id])
        return Document.search().filter(f)[offset:limit].execute()


    def save(self, **kwargs):
        self.created_at = datetime.now()
        retval = super().save(**kwargs)

        check_document_terms.delay(self.meta.id)

        return retval

Document.init()
