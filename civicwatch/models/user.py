from civicwatch import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from .feed import Feed

ROLES = {
    0: 'Disabled',
    1: 'User',
    99: 'Admin'
}

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128), )
    feeds = db.relationship('Feed', backref='user', foreign_keys=[Feed.user_id])
    role = db.Column(db.Integer, default=1)

    def __repr__(self):
        return "User(id={}, email='{}')".format(self.id, self.email)

    @property
    def is_admin(self):
        if self.role and self.role >= 99:
            return True
        return False

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)
