from datetime import datetime
from civicwatch import db
from civicwatch.scraper import scrape

class Crawl(db.Model):
    __tablename__ = 'crawls'
    id = db.Column(db.Integer, primary_key=True)
    source_id = db.Column(db.Integer, db.ForeignKey('sources.id'))
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "Crawl(id={}, url='{}')".format(self.id, self.source.url)

    def start(self):
        scrape.queue_url(self.source.url, crawl_id=self.id)

@db.event.listens_for(Crawl, 'after_insert')
def after_insert_listener(mapper, connection, target):
    target.start()
