from datetime import datetime
from civicwatch import db
from .result import Result
from civicwatch.tasks import initialize_term_results
from elasticsearch_dsl import connections

STATUS_VALUES = ['Uninitialized', 'In-Progress', 'Initialized']

class NormalizedTerm(db.Model):
    __tablename__ = 'normalizedterms'
    id = db.Column(db.Integer, primary_key=True)
    term = db.Column(db.String(64), index=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    status = db.Column(db.Integer, default=0)
    results = db.relationship('Result', backref='normalized_term', foreign_keys=[Result.normalized_term_id])

    def __repr__(self):
        return "NormalizedTerm(id={}, term='{}')".format(self.id, self.term)

    @staticmethod
    def normalize(term):
        term = term.lower().strip()

        es = connections.connections.get_connection()
        result = es.indices.analyze(index='', analyzer='english', text=term)
        if 'tokens' in result:
            tokens = result['tokens']
            term = ' '.join([t['token'] for t in tokens])

        return term

    @staticmethod
    def find_or_create(term):
        term = NormalizedTerm.normalize(term)

        normalized_term = NormalizedTerm.query.filter_by(term=term).first()
        if normalized_term:
            return normalized_term
        else:
            normalized_term = NormalizedTerm(term=term)
            return normalized_term

    def current_status(self):
        return STATUS_VALUES[self.status]

    def initialize_results(self):
        initialize_term_results.delay(self.id)


@db.event.listens_for(NormalizedTerm, 'after_insert')
def after_insert_listener(mapper, connection, target):
    try:
        target.initialize_results()
    except:
        'Failed to queue term initilization!'
