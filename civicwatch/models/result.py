from datetime import datetime
from civicwatch import db

class Result(db.Model):
    __tablename__ = 'results'
    id = db.Column(db.Integer, primary_key=True)
    highlight = db.Column(db.String(400), index=True)
    url = db.Column(db.String(300), index=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    date = db.Column(db.DateTime)

    normalized_term_id = db.Column(db.Integer, db.ForeignKey('normalizedterms.id'), index=True)

    source_id = db.Column(db.Integer, db.ForeignKey('sources.id'), index=True)
    source = db.relationship('Source', foreign_keys=[source_id])

    crawl_id = db.Column(db.Integer, db.ForeignKey('crawls.id'), index=True)
    crawl = db.relationship('Crawl', foreign_keys=[crawl_id])


    def __repr__(self):
        return "Result(id={}, highlight='{}')".format(self.id, self.highlight)

    def serialize(self):
        return dict(
            id=self.id,
            highlight=self.highlight,
            url=self.url,
            source_name=self.source.name,
            state=self.source.state,
            date=self.date.strftime("%s")
        )
