from datetime import datetime, timedelta
from civicwatch import db
from .crawl import Crawl

class Source(db.Model):
    __tablename__ = 'sources'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    url = db.Column(db.String(300), unique=True, index=True)
    state = db.Column(db.String(2))
    county = db.Column(db.String(64))
    crawls = db.relationship('Crawl', backref='source')
    last_run = db.Column(db.DateTime)
    next_run = db.Column(db.DateTime, default=datetime.utcnow)
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "Source(id={}, url='{}')".format(self.id, self.url or '')

    def crawl_now(self):
        crawl = Crawl(source=self)
        self.last_run = datetime.utcnow()
        self.next_run = datetime.utcnow() + timedelta(days=1)

        db.session.add(crawl)
        db.session.add(self)

        db.session.commit()
