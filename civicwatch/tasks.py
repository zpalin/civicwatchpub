from random import randint
from datetime import datetime
from . import celery, db

@celery.task()
def check_needed_crawls():
    from .models import Source, Crawl

    sources = Source.query.filter(Source.next_run <= datetime.utcnow()).all()

    for s in sources:
        s.crawl_now()


@celery.task(bind=True)
def initialize_term_results(self, normalized_term_id):
    from .models import NormalizedTerm
    from civicwatch.search.basic import get_hits_by_term

    try:
        normterm = NormalizedTerm.query.get(normalized_term_id)
        get_hits_by_term(normterm)
    except Exception as exc:
        retry_in = 2 ** self.request.retries
        print("Retrying in: ", retry_in)
        self.retry(exc=exc, countdown=retry_in)



@celery.task()
def check_document_terms(document_id):
    from .models import NormalizedTerm
    from civicwatch.search.basic import get_hits_by_term

    terms = NormalizedTerm.query.all()
    for t in terms:
        get_hits_by_term(t, doc_ids=[document_id])
