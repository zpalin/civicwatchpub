from flask_admin.contrib.sqla import ModelView
from flask_admin.base import MenuLink
from flask_admin import AdminIndexView, expose, Admin
from . import app, db, models
from flask_login import current_user
from flask import redirect, url_for, request


class AuthMixin(object):
    def is_accessible(self):
        return hasattr(current_user, 'is_admin') and current_user.is_admin

    def inaccessible_callback(self, name, **kwargs):
        return redirect('/dashboard')


class AdminModelView(AuthMixin, ModelView):
    pass


class AdminIndex(AuthMixin, AdminIndexView):
    @expose('/')
    def index(self):
        return self.render('admin/index.j2')


index_view=AdminIndex(url='/admin', name='Home')
admin_app = Admin(app, name='CivicWatch', template_mode='bootstrap3', index_view=index_view)

admin_app.add_view(AdminModelView(models.User, db.session, category='Accounts'))
admin_app.add_view(AdminModelView(models.Source, db.session))
admin_app.add_view(AdminModelView(models.Crawl, db.session))
admin_app.add_view(AdminModelView(models.Feed, db.session))
admin_app.add_view(AdminModelView(models.NormalizedTerm, db.session))
admin_app.add_view(AdminModelView(models.Term, db.session))
admin_app.add_view(AdminModelView(models.Result, db.session))

admin_app.add_link(MenuLink(name='App', url='/dashboard'))
admin_app.add_link(MenuLink(name='Logout', url='/'))
