from civicwatch.models import Document, Result, Crawl, Source
from civicwatch import db
from elasticsearch_dsl import Q
from collections import namedtuple
from urllib.parse import urlparse

def search_term(term):
    query_string = Q('match_phrase', body=term)

    s = Document.search() \
        .query(query_string)

    results = s.execute()
    return results

def check_valid_highlight(highlight):
    return len(highlight.split()) >= 6

def get_highlights(result):
    highlights = []

    for h in result.meta.highlight['body']:
        hlines = h.split('\n')
        start_line = -1
        end_line = -1

        for i, line in enumerate(hlines):
            if line.find('<em>') >= 0 and start_line == -1:
                start_line = i

            if line.find('</em>') >= 0:
                end_line = i

        highlight = '<br/>'.join(hlines[start_line:end_line+1])
        if check_valid_highlight(highlight):
            highlights.append(highlight)

    return highlights

def get_hits_by_term(normalized_term, doc_ids=[]):
    search = Document.search().filter(Q('match_phrase', body=normalized_term.term))
    search = search.highlight(
        'body',
        fragment_size=250,
        number_of_fragments=100
    )

    if doc_ids:
        search = search.filter(Q('ids', values=doc_ids))

    for result in search.scan():
        highlights = get_highlights(result)

        for h in highlights:
            create_result(normalized_term, result, h)


def get_source_by_url(url):
    domain = urlparse(url).netloc
    return Source.query.filter(Source.url.contains(domain)).first()

def create_result(normalized_term, document, text):
    crawl_id = int(document.crawl_id)

    source = None
    crawl = Crawl.query.filter_by(id=crawl_id).first()
    if crawl:
        source = crawl.source
    else:
        source = get_source_by_url(document.url)

    exists = Result.query.filter_by(normalized_term=normalized_term, highlight=text, source=source).first()

    if not exists:
        res = Result(normalized_term=normalized_term, highlight=text)
        res.date = document.created_at
        res.crawl = crawl
        res.source = source
        res.url = document.url
        db.session.add(res)
        db.session.commit()
