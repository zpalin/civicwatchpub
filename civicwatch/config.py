import os
from datetime import timedelta
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'use a better secret for prod'
    FILE_STAGING_DIR = os.path.join(basedir, '..', 'staging')
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    ELASTIC_SERVER = 'localhost'
    ELASTIC_USERNAME = 'elastic'
    ELASTIC_PASSWORD = 'elastic'

    CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672/'
    CELERYD_PREFETCH_MULTIPLIER = 24
    CELERYBEAT_SCHEDULE = {
        'check_for_crawls': {
            'task': 'civicwatch.tasks.check_needed_crawls',
            'schedule': timedelta(seconds=30)
        },
    }

    REDIS_URL = "redis://@localhost:6379/0"

    # Default Admin Credentials for new Environment
    LOGIN_EMAIL = 'admin@civicwatch.co'
    LOGIN_PASSWORD = 'civicwatch1'

    SCRAPE_TYPES = ['pdf']

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True

    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'postgresql://civicwatch:civicwatch@localhost:5432/civicwatch'

    SCRAPE_TYPES = [] # Disabling pdfs for faster testing

    ELASTIC_INDEX = 'civicwatch_dev'

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')
    ELASTIC_INDEX = 'civicwatch_test'

class ProductionConfig(Config):
    ELASTIC_INDEX = 'civicwatch_prod'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

    REDIS_URL = os.environ.get('REDIS_URL')
    CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL')

    ELASTIC_SERVER = os.environ.get('ELASTIC_SERVER')
    ELASTIC_USERNAME = os.environ.get('ELASTIC_USERNAME')
    ELASTIC_PASSWORD = os.environ.get('ELASTIC_PASSWORD')

config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
