const types = {
  FETCH: 'feeds/fetch',
  GET: 'feeds/get',
  LOAD_ONE: 'feeds/load one',
  LOAD: 'feeds/load',
  RESET: 'feeds/reset',
  UPDATE: 'feeds/update',
  CREATE: 'feeds/create',
  SET_VALUE: 'feeds/set value',
  ADD_TERM: 'feeds/add term',
  SET_TERM: 'feeds/set term',
  DELETE_TERM: 'feeds/delete term'
};

export default types;
