import reducer from './reducer';
import types from './types';
import * as actions from './actions';

const feeds = {
  types,
  reducer,
  actions
}

export default feeds;
