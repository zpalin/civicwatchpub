import { makeRequest } from '../utils';
import types from './types';

const url = '/api/feeds';

function createPayload(feed) {
  const data = {
    ...feed,
    results: [],
    terms: JSON.stringify(feed['terms'])
  };
  return JSON.stringify(data);
}

export function fetchFeeds(includeResults=true, callback=null) {
  return function(dispatch) {
    const type = types.FETCH;
    const reqUrl = `${url}?include_results=${includeResults}`;
    const options = { method: 'GET' };
    return makeRequest({ type, url: reqUrl, options, callback, dispatch });
  }
}


export function getFeed(feedId, callback=null) {
  return function(dispatch) {
    const type = types.GET;
    const reqUrl = `${url}/${feedId}`;
    const options = { method: 'GET' };
    return makeRequest({ type, url: reqUrl, options, callback, dispatch });
  }
}

export function updateFeed({ feed }, callback=null) {
  return function(dispatch) {
    const type = types.UPDATE;
    const reqUrl = `${url}/${feed.id}`;
    const options = {
      method: 'PUT',
      body: createPayload(feed)
    };
    return makeRequest({ type, url: reqUrl, options, callback, dispatch });
  }
}

export function createFeed({ feed }, callback=null) {
  return function(dispatch) {
    const type = types.CREATE;
    const reqUrl = `${url}`;
    const options = {
      method: 'POST',
      body: createPayload(feed),
    };
    return makeRequest({ type, url: reqUrl, options, callback, dispatch });
  }
}

export function deleteFeed(feedId, callback=null) {
  return function(dispatch) {
    const type = types.GET;
    const reqUrl = `${url}/${feedId}`;
    const options = { method: 'DELETE' };
    return makeRequest({ type, url: reqUrl, options, callback, dispatch });
  }
}

export function setFeedField({ field, value }) {
  return {
    type: types.SET_VALUE,
    field,
    value
  }
}

export function clearFeed() {
  return { type: types.RESET }
}

export function addTerm() {
  return { type: types.ADD_TERM }
}

export function setTerm({ termId, value }) {
  return { type: types.SET_TERM, termId, value }
}

export function deleteTerm({ termId }) {
  return { type: types.DELETE_TERM, termId }
}
