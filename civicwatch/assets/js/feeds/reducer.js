import types from './types';

const initialState = {
  // FETCH props
  items: [],
  isFetching: false,
  lastUpdated: 0,
  didInvalidate: true,
  fetchError: '',

  // GET props
  item: {},
  isFetchingItem: false,
  lastUpdatedItem: 0,
  didInvalidateItem: true,
  getError: '',

  // CREATE props
  isCreating: false,

  // UPDATE props
  isUpdating: false,

  // DELETE props
  isDeleting: false
};

export default function(state=initialState, action) {
  switch(action.type) {

    // Get multiple fetch records
    case types.FETCH:
      switch(action.status) {
        case 'pending':
          return {
            ...state,
            isFetching: true,
            didInvalidate: false,
            fetchingError: ''
          }
        case 'resolved':
          return {
            ...state,
            isFetching: false,
            didInvalidate: false,
            items: action.body,
            lastUpdated: action.receivedAt,
            fetchingError: ''
          }
        case 'rejected':
          return {
            ...state,
            isFetching: false,
            didInvalidate: false,
            fetchingError: ''
          }
      }

    // Get a single fetch record
    case types.GET:
      switch(action.status) {
        case 'pending':
          return {
            ...state,
            isFetchingItem: true,
            didInvalidate: false,
            getError: ''
          }
        case 'resolved':
          return {
            ...state,
            isFetchingItem: false,
            didInvalidateItem: false,
            item: action.body,
            lastUpdatedItem: action.receivedAt,
            getError: ''
          }
        case 'rejected':
          return {
            ...state,
            isFetchingItem: false,
            didInvalidateItem: false,
            getError: 'Error'
          }
      }

    // set value of a field on item
    case types.SET_VALUE:
      return {
        ...state,
        item: {
          ...state.item,
          [action.field]: action.value,
          didInvalidateItem: true
        }
      }

    case types.SET_TERM:
    case types.ADD_TERM:
    case types.DELETE_TERM:
      return {
        ...state,
        item: {
          ...state.item,
          terms: termsReducer(state.item.terms, action),
          didInvalidateItem: true
        }
      }

    case types.RESET:
      return initialState;
  }

  return state;
}

function termsReducer(state=[], action) {
  switch(action.type) {

    case types.SET_TERM:
      return state.map((term) => {
        if (term.id == action.termId)
          return {
            ...term,
            label: action.value
          }
        return term;
      });

    case types.ADD_TERM:
      return [
        ...state,
        {
          id: `temp-${Date.now().toString()}`,
          label: ''
        }
      ]

    case types.DELETE_TERM:
      return state.filter((term) => {
        if (term.id != action.termId)
          return true;
      });
  }

  return state;
}
