import feeds from './feeds';

const actions = {
  ...feeds.actions
}

export default actions;
