import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ES6Promise from 'es6-promise';
import 'whatwg-fetch';


if(typeof Promise === 'undefined') {
  ES6Promise.polyfill();
}

import Router from './router';
import Store from './store';

window.Store = Store;

ReactDOM.render(
  <Provider store={Store}>
    <Router />
  </Provider>
  , document.querySelector('.react-mount')
);
