import React from 'react';
import { formatTimeStamp } from '../utils';
import { Tooltip, OverlayTrigger, Button } from 'react-bootstrap';

const PaddedIcon = ({icon, href}) => {
  return (
    <Button href={href} className="paddedIcon">

    </Button>
  )
}

const I = ({icon}) => <i className={`fa fa-${icon}`} />

const TooltipWrap = ({ id, children, tooltipText }) => {
  const overlayTooltip = (
    <Tooltip id={id}>{tooltipText}</Tooltip>
  );
  return (
    <OverlayTrigger placement="top" overlay={overlayTooltip}>
      { children }
    </OverlayTrigger>
  )
}

export default ({ id, source_name, state, date, highlight, url }) => {
  return (
    <div className="feedCard">
      <div className="info">
        {`${source_name}, ${state}`} <span className="divider">·</span> {formatTimeStamp(date)}
      </div>

      <div className="body" dangerouslySetInnerHTML={{__html: highlight}} />

      <div className="actions">
        <Button className="paddedIcon">
          <I icon="star"/>
        </Button>

        <Button className="paddedIcon">
          <I icon="share"/>
        </Button>

        <TooltipWrap id={`link${id}`} tooltipText="View at source" >
          <Button href={url} target="_blank" className="paddedIcon">
            <I icon='link'/>
          </Button>
        </TooltipWrap>

        <Button className="paddedIcon">
          <I icon="ellipsis-v"/>
        </Button>
      </div>
    </div>
  );
}
