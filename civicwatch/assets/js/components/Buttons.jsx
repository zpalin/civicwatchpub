import React from 'react';
import { Link } from 'react-router';
import { Button, Tooltip, OverlayTrigger } from 'react-bootstrap';

export const IconLink = ({ className, icon, children, to }) => (
  <Link className={`btn btn-primary ${className}`} to={to}>
    <i className={`fa fa-${icon}`} />{' '}
    { children }
  </Link>
);

export const IconButton = ({ className, icon, children, onClick }) => (
  <Button className={`btn btn-primary ${className}`} onClick={onClick}>
    <i className={`fa fa-${icon}`} />{' '}
    { children }
  </Button>
);

export const TooltipIconButton = ({ tooltip, icon, className, to }) => {
  const iconElement = ( <i className={`fa fa-${icon}`} /> );
  const divClass = `iconButton ${className || ''}`;

  if (tooltip) {
    const tooltip = (
      <Tooltip id="tooltip">{tooltip}</Tooltip>
    );

    return (
      <div className={divClass}>
        <OverlayTrigger placement="top" overlay={tooltip}>
          <Link to={to}>
            <div className="buttonPadding">
              {iconElement}
            </div>
          </Link>
        </OverlayTrigger>
      </div>
    )

  } else {
    return (
      <div className={divClass}>
        <Link to={to}>
          <div className="buttonPadding">
            {iconElement}
          </div>
        </Link>
      </div>
    )
  }
}
