import React, { Component } from 'react';

import FeedCard from './FeedCard.jsx';
import { TooltipIconButton } from './Buttons.jsx';

export default ({ feed }) => {
  const { results=[] } = feed;
  return (
    <div className="feedPipeline">
      <div className="header">
        { feed.title || 'Untitled' } <TooltipIconButton className="menuButton" icon="ellipsis-v"/>
      </div>

      <div className="contents">
        { results.map(res => <FeedCard key={res.id} {...res} />) }
      </div>
    </div>
  )
}
