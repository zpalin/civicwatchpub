import React from 'react';

export default ({ buttons, title='', children }) => {
  let buttonType = '';
  if (!Array.isArray(buttons)) buttonType = 'create';

  return (
    <div className="feedsSidebar">
      <div className={`actions ${buttonType}`}>
        { buttons }
      </div>
      <div className="sidebarTitle">
        { title }
      </div>
      <div className="detailPanel">
        { children }
      </div>
    </div>
  )
}
