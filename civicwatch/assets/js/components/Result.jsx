import React, { Component } from 'react';
import { formatTimeStamp } from '../utils';

export const Result = ({ source_name, state, date, highlight, url }) => {

  return (
    <div className="resultItem">
      <div className="info">
        {`${source_name}, ${state}`} <span className="divider">·</span> {formatTimeStamp(date)}
      </div>

      <div className="actions">
        <a href={url} target="_blank" className="btn btn-primary">
          View
        </a>
      </div>

      <div className="body" dangerouslySetInnerHTML={{__html: highlight}} />
    </div>
  );
}

export default ({ results }) => {
  return (
    <div className="resultsList">
      { results.map(res => <Result key={res.id} {...res} />) }
    </div>
  );
}
