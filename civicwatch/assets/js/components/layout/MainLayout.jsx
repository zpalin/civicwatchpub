import React, { Component } from 'react';

import Navbar from './Navbar.jsx';

class MainLayout extends Component {  
  render() {
    document.title = "CivicWatch";
    return (
      <div className="site">
        <Navbar />
        <div className="container-fluid pageContents">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default MainLayout;
