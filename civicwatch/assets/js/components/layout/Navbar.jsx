import React, { Component, PropTypes } from 'react';
import { IndexLink, Link, browserHistory } from 'react-router';
import { Navbar, Nav, NavItem, MenuItem, Button, Input } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class NavbarComponent extends Component {
  render() {

    return (
      <Navbar staticTop inverse fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/dashboard"><img alt="CivicWatch" src="/static/img/CivicWatchLogo.png" /></Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>

        <Navbar.Collapse>

          <Nav>
            <LinkContainer to="/dashboard">
              <NavItem><i className="fa fa-tachometer"/> Dashboard</NavItem>
            </LinkContainer>
          </Nav>

          <Nav>
            <LinkContainer to="/feeds">
              <NavItem><i className="fa  fa-list-ul"/> Feeds</NavItem>
            </LinkContainer>
          </Nav>

          <Nav>
            <LinkContainer to="/settings">
              <NavItem><i className="fa  fa-cogs"/> Settings</NavItem>
            </LinkContainer>
          </Nav>

          <Nav pullRight>
            <NavItem href="/logout">Logout <i className="fa fa-sign-out"/></NavItem>
          </Nav>

        </Navbar.Collapse>
      </Navbar>
    );
  }

};

export default NavbarComponent;
