import React, { Component, PropTypes } from 'react';

import MainLayout from './layout/MainLayout.jsx';

class App extends Component {
  render() {
    return (
      <div>
        <MainLayout>
          {this.props.children}
        </MainLayout>
      </div>
    );
  }
}

export default App;
