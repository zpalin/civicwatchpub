import React from 'react';

export const ReadOnlyField = ({ label, children }) => {
  return (
    <div className="field">
      { label ? <span className="label">{ label }</span> : '' }
      <span className="value">
        { children }
      </span>
    </div>
  )
}

export const DropdownField = ({ label, options=[], value, onChange }) => {
  return (
    <div className="field form-group">
      { label ? <span className="label">{ label }</span> : '' }
      <select className="form-control" defaultValue={value} onChange={onChange}>
        { options.map(i => <option key={i.value} value={i.value}>{i.label}</option>) }
      </select>
    </div>
  );
}

export const TermField = ({ term, onChange, onDelete }) => {
  return (
    <div className="field form-group">
      <div className="input-group">
        <input type="text" className="form-control" value={term} onChange={onChange}/>
        <div className="input-group-addon" onClick={onDelete} >
          <i className="fa fa-times" />
        </div>
      </div>
    </div>
  );
}

export const CheckBox = ({ value, onClick }) => {
  const icon = value ? 'check-square-o' : 'square-o';
  return (
    <i onClick={() => { onClick && onClick() }} className={`fa fa-${icon}`}/>
  );
}


export const CheckBoxField = ({ label, value, onClick }) => {
  return (
    <div className="field">
      { label ? <span className="label">{ label }</span> : '' }
      <span className="value">
        <CheckBox value={value} onClick={onClick} />
      </span>
    </div>
  );
}

export const FormSection = ({ title, children, button }) => (
  <div className="section">
    <div className="title">{ title }{ button }</div>
    { children }
  </div>
);

export const Form = ({ children }) => (
  <div className="sidebarForm">
    { children }
  </div>
);
