import React from 'react';

import Result from './Result.jsx';

export default ({ children }) => {
  return (
    <div className="feedsBody">
      { children }
    </div>
  )
}
