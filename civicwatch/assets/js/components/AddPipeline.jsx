import React, { Component } from 'react';

import FeedCard from './FeedCard.jsx';

export default () => {
  return (
    <div className="feedPipeline addPipeline">
      <div className="header">
        Add a Feed
      </div>

      <div className="contents">
        <div className="feedCard addCard">
          <button className="btn btn-default">
            <i className="fa fa-plus"></i> Add feed to dashboard
          </button>
        </div>
      </div>
    </div>
  )
}
