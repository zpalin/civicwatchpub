import moment from 'moment';

export const makeRequest = function({ type, url, options, callback, dispatch }) {
  const opts = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    credentials: 'include',
    ...options
  }

  dispatch({ type, status: 'pending' });
  return fetch(url, opts)
    .then((res) => res.json())
    .then((res) => {
      dispatch({ type, status: 'resolved', body: res, receivedAt: Date.now() });
      return res;
    })
    .then((res) => callback && callback(res))
    .catch((err) => {
      dispatch({ type, status: 'rejected', receivedAt: Date.now() });
    });
}

export const formatTimeStamp = (timestamp) => {
  return moment(Number(timestamp)* 1000).format("MMM D");
}
