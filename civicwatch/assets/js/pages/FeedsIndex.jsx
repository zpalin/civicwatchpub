import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import actions from '../actions';

import FeedsSidebar from '../components/FeedsSidebar.jsx';
import FeedsBody from '../components/FeedsBody.jsx';
import { IconLink } from '../components/Buttons.jsx';





const FeedsLink = ({ feed }) => <Link to={`feeds/${feed.id}`}>{feed.title || 'Untitled'}</Link>

const renderFeedLinks = (feeds) => feeds.map(f => <FeedsLink key={f.id} feed={f} />);
const createButton = <IconLink icon='plus' to='/feeds/create'>Create New</IconLink>;

class FeedsIndex extends Component {
  componentWillMount() {
    this.props.fetchFeeds();
  }

  render() {
    document.title = "CivicWatch | Feeds";
    const { feeds } = this.props;


    return (
      <div className="feedsPage">
        <FeedsSidebar title="FEEDS" buttons={createButton}>
          <div className="feedsList">
            { renderFeedLinks(feeds) }
          </div>
        </FeedsSidebar>
        <FeedsBody>
          <div className="instructions">
            Select Feed from Sidebar
          </div>
        </FeedsBody>
      </div>
    )
  }
}

const bindState = (state) => {
  return {
    feeds: state.feeds.items
  }
}
export default connect(bindState, actions)(FeedsIndex);
