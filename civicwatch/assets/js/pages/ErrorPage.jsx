import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

export default () => {
  document.title = "CivicWatch | Not Found";
  
  return (
    <div className="errorPage">
      <i className="fa fa-wrench"></i>

      <div className="message">
        <span className="number">
          404
        </span>
        <span className="divider"></span>
        <span className="description">
          Page Not Found
        </span>
      </div>


      <LinkContainer to="/dashboard">
        <Button className="purple">Back to Dashboard</Button>
      </LinkContainer>
    </div>
  )
}
