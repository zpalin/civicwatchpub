import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import actions from '../actions';

import FeedPipeline from '../components/FeedPipeline.jsx';
import AddPipeline from '../components/AddPipeline.jsx';
import { IconLink } from '../components/Buttons.jsx';


const FeedsLink = ({ feed }) => <Link to={`feeds/${feed.id}`}>{feed.title}</Link>

const renderFeedLinks = (feeds) => feeds.map(f => <FeedsLink key={f.id} feed={f} />);
const createButton = <IconLink icon='plus' to='/feeds/create'>Create New</IconLink>;

class Dashboard extends Component {
  componentWillMount() {
    this.props.fetchFeeds();
  }

  render() {
    document.title = "CivicWatch | Dashboard";
    const { feeds } = this.props;
    const pipelineCount = feeds.length;

    const scrollContainerWidth = 375 * (pipelineCount);
    return (
      <div className="dashboardPage">
        <div className="scrollContainer" style={{width: scrollContainerWidth}}>
          { feeds.map((f) => <FeedPipeline key={f.id} feed={f}/> ) }
          {/* <AddPipeline /> */}
        </div>
      </div>
    )
  }
}

const bindState = (state) => {
  return {
    feeds: state.feeds.items
  }
}
export default connect(bindState, actions)(Dashboard);
