import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import actions from '../actions';


import FeedsSidebar from '../components/FeedsSidebar.jsx';
import FeedsBody from '../components/FeedsBody.jsx';
import { Form, TermField, CheckBoxField, DropdownField, ReadOnlyField, FormSection } from '../components/SidebarForm.jsx';
import { IconLink, IconButton } from '../components/Buttons.jsx';
import { ALERT_METHOD_OPTIONS, ALERT_FREQUENCY_OPTIONS } from '../constants';

function goToDetails({ id }) {
  browserHistory.push(`/feeds/${id}`);
}

class FeedsEdit extends Component {
  constructor() {
    super();

    this.state = {
      mode: 'edit'
    }

    this.onSave = this.onSave.bind(this);
  }

  componentWillMount() {
    const { id } = this.props.params;
    if (id) {
      if (this.props.feed.id !== id)
        this.props.getFeed(id);
    } else {
      this.setState({ mode: 'create' });
      this.props.clearFeed();
    }
  }

  onSave() {
    const { mode } = this.state;
    const { feed } = this.props;

    if (mode == 'edit') {
      this.props.updateFeed({feed}, goToDetails);
    } else {
      this.props.createFeed({feed}, goToDetails);
    }
  }

  renderButtons() {
    const { mode } = this.state;
    const { id } = this.props.feed;

    const cancelLink = (mode == 'edit') ? `/feeds/${ id }` : '/feeds';

    return [
      (<IconLink key='cancelButton' to={cancelLink} icon='ban' className='cancel'>Cancel</IconLink>),
      (<IconButton key='saveButton' icon='floppy-o' onClick={this.onSave}>Save</IconButton>)
    ]
  }

  renderTitle() {
    const { mode } = this.state;
    const { title='' } = this.props.feed;

    return (
      <input
        type="text"
        className='form-control'
        placeholder='Enter a feed name'
        value={title}
        onChange={
          (e) => this.props.setFeedField({field: 'title', value: e.target.value})
        }
      />
    );
  }

  renderTerms(terms=[]) {
    const { setTerm, deleteTerm } = this.props;

    return terms.map((t) => {
      return (
        <TermField
          key={t.id}
          term={t.label}
          onChange={ (e) => setTerm({termId: t.id, value: e.target.value}) }
          onDelete={ (e) => deleteTerm({termId: t.id}) }
        />
      )
    });
  }

  renderAddTermButton() {
    return (
      <IconButton icon='plus' onClick={() => this.props.addTerm()}>
        Add
      </IconButton>
    )
  }

  render() {
    const { mode } = this.state;
    const { setFeedField, feed } = this.props;
    const { id, title, alert, terms=[] } = feed;

    if (mode == 'edit') {
      document.title = `CivicWatch | ${title || 'Feeds'}`;
    } else {
      document.title = "CivicWatch | Create Feed";
    }



    return (
      <div className="feedsPage">
        <FeedsSidebar title={this.renderTitle()} buttons={this.renderButtons()} >
          <Form>
            <FormSection title="Alerts">

              <CheckBoxField
                label='Enabled'
                value={alert}
                onClick={() => setFeedField({field: 'alert', value: !alert})}
              />
              <DropdownField label='Frequency' options={ALERT_FREQUENCY_OPTIONS} value='daily' />
              <DropdownField label='Method' options={ALERT_METHOD_OPTIONS} value='email' />

            </FormSection>
            <FormSection title="Terms" button={this.renderAddTermButton()}>
              { this.renderTerms(terms) }
            </FormSection>
          </Form>
        </FeedsSidebar>
        <FeedsBody>
          {
            (mode == 'edit')
              ? <div className="instructions">Select Feed from Sidebar</div>
            : ''
          }
        </FeedsBody>
      </div>
    );
  }
}

const bindState = (state) => {
  return {
    feed: state.feeds.item
  }
}
export default connect(bindState, actions)(FeedsEdit);
