import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import actions from '../actions';


import FeedsSidebar from '../components/FeedsSidebar.jsx';
import FeedsBody from '../components/FeedsBody.jsx';
import { Form, ReadOnlyField, CheckBox, FormSection } from '../components/SidebarForm.jsx';
import { IconLink } from '../components/Buttons.jsx';
import ResultList from '../components/Result.jsx';

const TermsList = ({ terms=[] }) => terms.map( t => <ReadOnlyField key={t.id} label={t.label} />);


const renderButtons = ({ id }) => [
    (<IconLink key='backButton' to='/feeds' icon='chevron-left'>Back</IconLink>),
    (<IconLink key='editButton' to={`/feeds/${ id }/edit`} icon='pencil-square-o'>Edit</IconLink>)
]

class FeedsDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      interval: null
    };
  }

  componentWillMount() {
    const { id } = this.props.params;
    this.props.clearFeed();    
    this.props.getFeed(id);

    const autoRefresh = setInterval(() => {
      console.log("Refreshing...");
      this.props.getFeed(id);
    }, 3000);

    this.setState({
      interval: autoRefresh
    });
  }

  componentWillUnmount() {
    const { interval } = this.state;
    interval && clearInterval(interval);
  }

  render() {
    const feed = this.props.feed || {};
    const { id, title, alert, terms=[], results=[] } = feed;
    document.title = `CivicWatch | ${title || 'Feeds'}`;
    const sidebarTitle = title || 'Untitled';

    return (
      <div className="feedsPage">
        <FeedsSidebar title={sidebarTitle.toUpperCase()} buttons={renderButtons(feed)} >
          <Form>
            <FormSection title="Alerts">
              <ReadOnlyField label='Enabled'><CheckBox value={alert} /></ReadOnlyField>
              <ReadOnlyField label='Frequency'>Daily</ReadOnlyField>
              <ReadOnlyField label='Method'>Email</ReadOnlyField>
            </FormSection>
            <FormSection title="Terms">
              { TermsList({ terms }) }
            </FormSection>
          </Form>
        </FeedsSidebar>

        <FeedsBody>
          <ResultList results={results} />
        </FeedsBody>
      </div>
    )
  }
}

const bindState = (state) => {
  return {
    feed: state.feeds.item,
    feeds: state.feeds.items
  }
}
export default connect(bindState, actions)(FeedsDetail);
