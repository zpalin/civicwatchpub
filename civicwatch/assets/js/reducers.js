import { combineReducers } from 'redux';
import feedsReducer from './feeds/reducer';

const rootReducer = combineReducers({
  feeds: feedsReducer
});

export default rootReducer;
