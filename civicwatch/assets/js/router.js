import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import App from './components/App.jsx';

import Dashboard from './pages/Dashboard.jsx';

import FeedsIndex from './pages/FeedsIndex.jsx';
import FeedsDetail from './pages/FeedsDetail.jsx';
import FeedsEdit from './pages/FeedsEdit.jsx';

import ErrorPage from './pages/ErrorPage.jsx';

export default () => {
  return (
    <Router history={browserHistory}>
      <Route path='/' component={App}>
        <Route path='dashboard' component={Dashboard} />

        <Route path='feeds/create' component={FeedsEdit} />
        <Route path='feeds/:id/edit' component={FeedsEdit} />
        <Route path='feeds/:id' component={FeedsDetail} />
        <Route path='feeds' component={FeedsIndex} />

        <Route path='*' component={ErrorPage} />
      </Route>
    </Router>
  );
}
