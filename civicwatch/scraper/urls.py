from urllib.parse import urlparse, urljoin
from os.path import splitext

IGNORED_EXTENSIONS = ['.mp3', '.wav', '.mp4', '.wma', '.mov']


def normalize_url(url, base_url=""):
    if url and url[0] == '#':
        return ''
    parsed_url = urlparse(url)
    parsed_base = urlparse(base_url)

    # if domain is blank inherit from the base url
    if not parsed_url.netloc:
        if base_url:
            try:
                url = parsed_url.geturl()
                url = urljoin(base_url, url)
                parsed_url = urlparse(url)
            except:
                return ''

    if not parsed_url.scheme:
        parsed_url = parsed_url._replace(scheme=parsed_base.scheme)
    elif parsed_url.scheme.lower() == 'javascript':
        return ''

    if parsed_url.netloc != parsed_base.netloc:
        return ''

    url = parsed_url.geturl()

    if bad_extension(url):
        return ''

    return url

def bad_extension(url):
    parsed_url = urlparse(url)
    _, extension = splitext(parsed_url.path)

    if extension.lower() in IGNORED_EXTENSIONS:
        return True

    return False
