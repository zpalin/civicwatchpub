import re
from bs4 import BeautifulSoup
from .urls import normalize_url
from . import files
import textract


def remove_elements_by_type(section, element_type):
    """ Removes elements with type element_type from section. """
    tags = section.find_all(element_type)
    [tag.decompose for tag in tags]


def extract_urls(body, base_url):
    a_tags = body.find_all('a')

    urls = []
    for tag in a_tags:
        url = str(tag.get('href', ''))
        url = normalize_url(url, base_url=base_url)
        if url:
            urls.append(url)

    return urls

def extract_text_from_file(filename):
    try:
        text = textract.process(filename)
    except:
        return ''

    if isinstance(text, bytes):
        text = text.decode()

    return text


def clean_text(text):
    lines = text.split('\n')
    text = '\n'.join([l.strip() for l in lines if l.strip()])
    return text

def handle_html(url, html):
    page = BeautifulSoup(html, 'lxml')
    body = page.body

    scripts = body.find_all('script')
    for s in scripts:
        s.decompose()

    noscripts = body.find_all('script')
    for s in noscripts:
        s.decompose()

    urls = extract_urls(body, url)

    text = re.sub(r'[\r\t]', ' ', body.text)
    text = '\n'.join([i.strip() for i in text.split('\n') if re.sub(r'\s\s+', '', i) and i.replace(' ', '')])

    return (text, urls)

def handle_downloadable(url, filetype):
    filename = files.download_file(url, filetype, retry_limit=5)
    text = extract_text_from_file(filename)
    files.delete_file(filename)

    return clean_text(text)
