import difflib
import time
import requests
from civicwatch import redis_store
from civicwatch.models import Document
import chardet
from .extract import handle_html, handle_downloadable
from civicwatch import celery
from elasticsearch.exceptions import SerializationError
from urllib.parse import urlparse
from civicwatch import app
from elasticsearch_dsl import Q


def queue_url(url, crawl_id, ref_url=''):
    crawl_id = str(crawl_id)

    prev_id = redis_store.get(url) or b''
    prev_id = prev_id.decode()

    if prev_id != crawl_id:
        print("Queued: {}".format(url))
        redis_store.set(url, crawl_id)
        crawl_page.delay(url, crawl_id=crawl_id, ref_url=ref_url)


def bad_redirect(url, redirect_url, crawl_id):
    if url == redirect_url:
        return False

    parsed_url = urlparse(url)
    parsed_redirect = urlparse(redirect_url)

    # If we navigated away from current domain with redirect
    if parsed_url.netloc != parsed_redirect.netloc:
        return True

    # if we've already seen this redirect url then skip it
    # otherwise set it in redis and crawl
    prev_id = redis_store.get(redirect_url) or b''
    prev_id = prev_id.decode()
    if prev_id == crawl_id:
        return True
    else:
        redis_store.set(redirect_url, crawl_id)
        return False


@celery.task(queue='crawls')
def crawl_page(url,  crawl_id, ref_url="", follow_links=True):
    print("Crawling page: {}".format(url))

    resp = requests.get(url, stream=True)

    if bad_redirect(url, resp.url, crawl_id):
        return
    else:
        url = resp.url

    content_type = resp.headers.get('Content-Type', '').lower()

    text = ""
    doc_type = ""
    source = ""
    scrape_types = app.config['SCRAPE_TYPES']
    if 'pdf' in content_type:
        if 'pdf' in scrape_types:
            #handle pdf
            doc_type = "pdf"
            text = handle_downloadable(url, "pdf")

    elif 'xlsx' in content_type:
        if 'xlsx' in scrape_types:
            #handle excel
            doc_type = "excel"

    elif 'docx' in content_type:
        if 'docx' in scrape_types:
            #handle word doc
            doc_type = "word"

    elif 'html' in content_type:
        source = resp.content
        encoding = chardet.detect(source)['encoding']
        source = source.decode(encoding)

        doc_type = "html"
        text, urls = handle_html(url, html=source)

        if follow_links:
            for u in urls:
                queue_url(url=u, crawl_id=crawl_id, ref_url=url)

    else:
        print("Skipping uknown content type: {}".format(content_type))
        return

    resp.close()

    data = {}
    data['url'] = url
    data['crawl_id'] = crawl_id
    data['doc_type'] = doc_type
    data['body'] = text

    if text:
        save_crawl(data)

    print("Finished Crawl: {}".format(url))
    return data


def save_crawl(data):
    doc = Document(**data)

    res = Document.search().filter(Q('match', url=doc.url)).sort('-created_at')[0:1].execute()
    if len(res):
        previous = res[0]
        if previous.body != doc.body:
            doc.save()
    else:
        doc.save()

def crawl_site(start_url):
    crawl_id = str(int(time.time() * 1000))
    queue_url(url=start_url, crawl_id=crawl_id, ref_url='')
