import os
import uuid
import urllib
import shutil
from civicwatch.config import basedir
from civicwatch import config_object


def ensure_directory_exists(dir):
    os.makedirs(dir, exist_ok=True)

def unique_filename(ext):
    ensure_directory_exists(config_object.FILE_STAGING_DIR)
    filename = "{}.{}".format(uuid.uuid4(), ext)
    return os.path.join(config_object.FILE_STAGING_DIR, filename)

def delete_file(filename):
    try:
        os.remove(filename)
    except FileNotFoundError:
        pass

def download_file(url, filetype, retry_limit=0):
    filename = unique_filename(filetype)

    retry = 0
    while True:
        try:
            with urllib.request.urlopen(url) as response, open(filename, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)
        except:
            retry += 1
            if retry > retry_limit:
                raise
        break

    return filename
